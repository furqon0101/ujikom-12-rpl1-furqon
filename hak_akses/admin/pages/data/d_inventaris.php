<?php
  session_start();
  if(!isset($_SESSION['username'])){
    echo '<script>document.location.href="index";</script>';
  }
?>
<?php
error_reporting(0);
if (isset($_SESSION['id_level']))
{
    // jika level admin
    if ($_SESSION['id_level'] == "3")
   {   
   }
   // jika kondisi level karyawan maka akan diarahkan ke halaman lain
   else if ($_SESSION['id_level'] == "2")
   {
       header('location:operator/index.php');
   }
}
if (!isset($_SESSION['id_level']))
{
    header('location:../../../index.php');
}
?>
<?php
	include ('modul/header.php');
?>

  <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Data Inventaris</h1>
			<ol class="breadcrumb">
			   <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
			   <li class="active">Data Inventaris</li>
			</ol>
		</section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default">+Tambah Data</button>
              <button class="btn btn-info" data-toggle="modal" data-target="#modal-info">Print PDF</button>
		        <div class="modal modal-info fade" id="modal-info">
		          <div class="modal-dialog">
		            <div class="modal-content">
		              <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  <span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">INFORMASI</h4>
		              </div>
		              <div class="modal-body">
		                <p>Anda Akan di Arahkan ke Halaman Download! Lanjutkan?&hellip;</p>
		              </div>
		              <div class="modal-footer">
		                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		                <a href="pdf/cetak_inv.php"><button type="button" class="btn btn-outline">Lanjutkan..</button></a>
		              </div>
		            </div>
		          </div>
		        </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
			<div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
					<tr>
					  <th class="text-center" style="margin:10px">No</th>
					  <th class="text-center">Nama</th>
					  <th class="text-center">Spesifikasi</th>
					  <th class="text-center">Kondisi</th>
					  <th class="text-center">Keterangan</th>
					  <th class="text-center">Jumlah</th>
					  <th class="text-center">Nama Jenis</th>
					  <th class="text-center">Tanggal Masuk</th>
					  <th class="text-center">Ruang</th>
					  <th class="text-center">Kode Inventaris</th>
					  <th class="text-center">Sumber</th>
					  <th class="text-center">Nama Petugas</th>
					  <th class="text-center">Opsi</th>
					</tr>
                </thead>
				
                <tbody>
                <?php
                    include "konek.php";
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM tb_inventaris INNER JOIN tb_jenis on tb_inventaris.id_jenis = tb_jenis.id_jenis 
					INNER JOIN tb_ruang on tb_inventaris.id_ruang = tb_ruang.id_ruang INNER JOIN tb_petugas on tb_inventaris.id_petugas = tb_petugas.id_petugas 
					ORDER BY id_inventaris DESC") or die (mysqli_error());
                    $i = 1;
                    while($data = mysqli_fetch_array($query_mysql)){
                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['nama']; ?></td>
                                    <td class="text-center"><?php echo $data['spesifikasi']; ?></td>
                                    <td class="text-center"><?php echo $data['kondisi']; ?></td>
                                    <td class="text-center"><?php echo $data['keterangan']; ?></td>
                                    <td class="text-center"><?php echo $data['jumlah']; ?></td>
                                    <td class="text-center"><?php echo $data['nama_jenis']; ?></td>
                                    <td class="text-center"><?php echo date ('d F Y', strtotime($data['tanggal_register'])) ?></td>
                                    <td class="text-center"><?php echo $data['nama_ruang']; ?></td>
                                    <td class="text-center"><?php echo $data['kode_inventaris']; ?></td>
                                    <td class="text-center"><?php echo $data['sumber']; ?></td>
                                    <td class="text-center"><?php echo $data['nama_petugas']; ?></td>
									<td class="text-center">
                                        <a href="#" data-toggle="modal" data-target="#modal-edit<?php echo $data['id_inventaris'];?>" class="btn btn-success">Ubah</a>
                                    </td>
                                </tr>
								
			<div class="modal fade" id="modal-edit<?php echo $data['id_inventaris'];?>">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Inventaris</h4>
					  <div class="modal-body">
						<form method="POST" action="u_inventaris.php">
						  <div class="box-body">
							<input type="hidden" name="id_inventaris" value="<?php echo $data['id_inventaris'] ?>" required>
							<div class="form-group">
							  <label>Nama Barang :</label>
							  <input name="nama" type="text" class="form-control" placeholder="" value="<?php echo $data['nama'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Spesifikasi :</label>
							  <input name="spesifikasi" type="text" class="form-control" placeholder="" value="<?php echo $data['spesifikasi'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Kondisi :</label>
								<select name="kondisi" class="form-control" >
									<option value="<?php echo $data['kondisi'] ?>"><?php echo $data['kondisi'] ?></option>
									<option value="baik">Baik</option>
									<option value="rusak">Rusak</option>
								</select>
							</div>
							<div class="form-group">
							  <label>Keterangan :</label>
							  <input name="keterangan" type="text" class="form-control" placeholder="" value="<?php echo $data['keterangan'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Jumlah :</label>
							  <input name="jumlah" type="number" class="form-control" placeholder="" value="<?php echo $data['jumlah'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Jenis :</label>
								<select name="id_jenis" class="form-control" value="<?php echo $data['id_jenis'] ?>">
									<?php
										$select=mysqli_query($konek, "SELECT * FROM tb_jenis");
										while($show=mysqli_fetch_array($select)){
									?>
										<option value="<?=$show['id_jenis'];?>"><?=$show['nama_jenis'];?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
							  <label>Tanggal Masuk :</label>
							  <input name="tanggal_register" type="date" class="form-control" placeholder="" value="<?php echo $data['tanggal_register'] ?>" readonly>
							</div>
							<div class="form-group">
							  <label>Ruang :</label>
								<select name="id_ruang" class="form-control" value="<?php echo $data['id_ruang'] ?>">
									<?php
										$select=mysqli_query($konek, "SELECT * FROM tb_ruang");
										while($show=mysqli_fetch_array($select)){
									?>
										<option value="<?=$show['id_ruang'];?>"><?=$show['nama_ruang'];?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
							  <label>Kode Inventaris :</label>
							  <input name="kode_inventaris" type="text" class="form-control" placeholder="" value="<?php echo $data['kode_inventaris'] ?>" readonly>
							</div>
							<div class="form-group">
							  <label>Sumber :</label>
							  <input name="sumber" type="text" class="form-control" placeholder="" value="<?php echo $data['sumber'] ?>" readonly>
							</div>
							<div class="form-group">
							  <label>ID Petugas :</label>
							  <input name="id_petugas" type="text" class="form-control" placeholder="" value="<?php echo $data['id_petugas'] ?>" readonly>
							</div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary" value="update">Simpan Perubahan</button>
						  </div>
						</form>
					  </div>
				    </div>
				  </div>
				</div>
                                <?php
                                    }
                                ?>
                </tbody>
              </table>

              
			<div class="modal fade" id="modal-default">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Inventaris</h4>
				  </div>
				  <div class="modal-body">			  
					<form method="POST" action="prostamb_inventaris.php">
					  <div class="box-body">
						<div class="form-group">
						  <label>Nama Barang :</label>
						  <input name="nama" type="text" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Spesifikasi :</label>
						  <input name="spesifikasi" type="text" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Kondisi :</label>
								<select name="kondisi" class="form-control" >
									<option>-</option>
									<option value="baik">Baik</option>
									<option value="rusak">Rusak</option>
								</select>
							</div>
						</div>
						<div class="form-group">
						  <label>Keterangan :</label>
						  <input name="keterangan" type="text" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Jumlah :</label>
						  <input name="jumlah" type="number" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Nama Jenis :  </label>
							<select name="id_jenis" class="form-control" required>
							<option>-</option>
								<?php     
									include"konek.php";
									$select=mysqli_query($konek, "SELECT * FROM tb_jenis");
									while($show=mysqli_fetch_array($select)){
								?>
									<option value="<?=$show['id_jenis'];?>"><?=$show['nama_jenis'];?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
						  <label>Ruang :  </label>
							<select name="id_ruang" class="form-control" required>
							<option>-</option>
								<?php     
									include"konek.php";
									$select=mysqli_query($konek, "SELECT * FROM tb_ruang");
									while($show=mysqli_fetch_array($select)){
								?>
									<option value="<?=$show['id_ruang'];?>"><?=$show['nama_ruang'];?></option>
								<?php } ?>
							</select>
						</div>
						<div class="form-group">
                            <?php
                              include "konek.php";
                              $cari_kd =mysqli_query($konek, "SELECT MAX(kode_inventaris) AS kode FROM tb_inventaris");
                                //besar atau kode yang baru masuk
                              $tm_cari = mysqli_fetch_array($cari_kd);
                              $kode = substr($tm_cari[0],4);
                              $tambah = $kode+1;
                                if ($tambah<10){
                                $kode_inventaris ="IS000".$tambah;
                                }else{
                                $kode_inventaris="IS00".$tambah;
                                }
                            ?>
						  <label>Kode Inventaris :  </label>
						  <input name="kode_inventaris" type="text" class="form-control" value="<?php echo $kode_inventaris; ?>" readonly="" required>
						</div>
						<div class="form-group">
						  <label>Sumber :</label>
						  <input name="sumber" type="text" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Petugas :  </label>
							<select name="id_petugas" class="form-control" required>
							<option>-</option>
								<?php     
									$select=mysqli_query($konek, "SELECT * FROM tb_petugas");
									while($show=mysqli_fetch_array($select)){
								?>
									<option value="<?=$show['id_petugas'];?>"><?=$show['nama_petugas'];?></option>
								<?php } ?>
							</select>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary" value="Simpan">Simpan</button>
					  </div>
					</form>
				  </div>
				</div>
			  </div>
			</div>
          </div>
          </div>
		</div>
    </section>
  </div>

<?php
include('modul/footer.php');
?>