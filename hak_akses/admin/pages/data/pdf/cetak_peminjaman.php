<?php
include 'konek.php';
require('fpdf.php');

date_default_timezone_set("Asia/Jakarta");
$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('lowe.jpg',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'DATA PEMINJAMAN',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telp : (0251)8631261',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. Raya Laladon, Kec. Ciomas, Kab.Bogor 16610',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Website : www.smkn1ciomas.sch.id, Email : smkn1_ciomas@yahoo.co.id',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"LAPORAN DATA PEMINJAMAN",0,10,'C');
$pdf->ln(0.5);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date('D-d/m/Y H:i:s'),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'No.', 1, 0, 'C');
$pdf->Cell(7, 0.8, 'Tanggal Pinjam', 1, 0, 'C');
$pdf->Cell(7, 0.8, 'Tanggal Kembali', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Status Peminjaman', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Nama Peminjam', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($konek, "SELECT * FROM tb_peminjaman INNER JOIN tb_pegawai on tb_peminjaman.id_pegawai = tb_pegawai.id_pegawai ORDER BY id_peminjaman DESC");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(7, 0.8, $lihat['tanggal_pinjam'],1, 0, 'C');
	$pdf->Cell(7, 0.8, $lihat['tanggal_kembali'], 1, 0,'C');
	$pdf->Cell(6, 0.8, $lihat['status_peminjaman'],1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['nama_pegawai'],1, 1,'C');

	$no++;
}

$pdf->Output("laporan_inventaris.pdf","I");

?>

