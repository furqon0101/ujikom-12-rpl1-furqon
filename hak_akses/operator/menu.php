  <header class="main-header">
      <!-- Logo -->
      <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>I</b>NV</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>INVENTARIS</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>


        <div class="navbar-custom-menu">

      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
     <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../peminjam/dist/img/p.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Operator</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </br>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Main Navigation</li>
        <li><a><i class="fa fa-calendar"></i><span><?php echo date("d-M-Y");?></span></a></li>
        <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
        <li class="header">Lainnya</li>
        <li><a href="minjam.php"><i class="fa fa-cube"></i> <span>Minjam Barang</span></a></li>
        <li><a href="d_pinjaman.php"><i class="fa fa-cubes"></i> <span>Data Pinjaman</span></a></li>
        <li><a href="d_inventaris.php"><i class="fa fa-barcode"></i> <span>Data Inventaris</span></a></li>
        <li><a href="logout.php"><i class="fa fa-sign-out"></i> <span>Keluar</span></a></li>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>