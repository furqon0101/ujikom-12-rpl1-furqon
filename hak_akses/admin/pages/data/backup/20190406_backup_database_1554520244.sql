DROP TABLE tb_detail_pinjam;

CREATE TABLE `tb_detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp` varchar(50) NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  KEY `id_peminjaman` (`id_peminjaman`),
  CONSTRAINT `tb_detail_pinjam_ibfk_2` FOREIGN KEY (`id_peminjaman`) REFERENCES `tb_peminjaman` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_detail_pinjam_ibfk_3` FOREIGN KEY (`id_inventaris`) REFERENCES `tb_inventaris` (`id_inventaris`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

INSERT INTO tb_detail_pinjam VALUES("1","53","1","dikembalikan","98");
INSERT INTO tb_detail_pinjam VALUES("2","52","5","dikembalikan","99");
INSERT INTO tb_detail_pinjam VALUES("3","53","5","dipinjam","98");
INSERT INTO tb_detail_pinjam VALUES("4","52","5","dikembalikan","100");
INSERT INTO tb_detail_pinjam VALUES("5","52","1","dikembalikan","101");
INSERT INTO tb_detail_pinjam VALUES("6","52","1","dikembalikan","102");
INSERT INTO tb_detail_pinjam VALUES("7","54","1","dikembalikan","102");
INSERT INTO tb_detail_pinjam VALUES("8","52","5","dikembalikan","106");
INSERT INTO tb_detail_pinjam VALUES("9","54","90","dikembalikan","107");
INSERT INTO tb_detail_pinjam VALUES("10","52","1","dipinjam","100");
INSERT INTO tb_detail_pinjam VALUES("11","53","1","dikembalikan","108");
INSERT INTO tb_detail_pinjam VALUES("12","52","4","dikembalikan","108");
INSERT INTO tb_detail_pinjam VALUES("13","53","1","dikembalikan","110");
INSERT INTO tb_detail_pinjam VALUES("14","52","23","dipinjam","110");
INSERT INTO tb_detail_pinjam VALUES("15","52","23","dikembalikan","111");
INSERT INTO tb_detail_pinjam VALUES("16","52","-1","dipinjam","112");
INSERT INTO tb_detail_pinjam VALUES("17","52","-2","dipinjam","112");
INSERT INTO tb_detail_pinjam VALUES("18","52","-4","dipinjam","112");
INSERT INTO tb_detail_pinjam VALUES("19","52","-8","dipinjam","112");
INSERT INTO tb_detail_pinjam VALUES("20","52","6","dipinjam","114");
INSERT INTO tb_detail_pinjam VALUES("21","52","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("22","52","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("23","52","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("24","52","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("25","54","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("26","54","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("27","52","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("28","52","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("29","53","1","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("30","54","2","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("31","54","3","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("32","54","4","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("33","54","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("34","54","5","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("35","54","6","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("36","52","7","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("37","52","8","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("38","52","9","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("39","54","10","dikembalikan","116");
INSERT INTO tb_detail_pinjam VALUES("40","53","3","dikembalikan","116");



DROP TABLE tb_inventaris;

CREATE TABLE `tb_inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `spesifikasi` varchar(60) NOT NULL,
  `kondisi` enum('baik','rusak') NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(15) NOT NULL,
  `sumber` varchar(60) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `tb_inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `tb_jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `tb_ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `tb_petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

INSERT INTO tb_inventaris VALUES("52","Laptop","AMD","baik","Kondisi Bagus","150","1","2019-02-26","1","IS0003","Pemerintah","38");
INSERT INTO tb_inventaris VALUES("53","Meja","Furniture","rusak","Bejad satu coy","4","3","2019-02-27","3","IS0002","Pemerintah","38");
INSERT INTO tb_inventaris VALUES("54","Bangku","Samsung","baik","Tersedia","140","1","2019-03-08","1","IS0001","Pemerintah","38");



DROP TABLE tb_jenis;

CREATE TABLE `tb_jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(15) NOT NULL,
  `ket` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO tb_jenis VALUES("1","Elekronik","JNS001","Elektronik");
INSERT INTO tb_jenis VALUES("2","Kelas","JNS002","Kelas");
INSERT INTO tb_jenis VALUES("3","Furniture","JNS003","Furniture");



DROP TABLE tb_level;

CREATE TABLE `tb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO tb_level VALUES("1","Peminjam");
INSERT INTO tb_level VALUES("2","Operator");
INSERT INTO tb_level VALUES("3","Admin");



DROP TABLE tb_pegawai;

CREATE TABLE `tb_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO tb_pegawai VALUES("4","furqon","furqon","Furqon","02100934","Villa Ciomas");
INSERT INTO tb_pegawai VALUES("5","pegawai","pegawai","Muhammad Ade Rohayat","021304566","Nuansa Asri Laladon");



DROP TABLE tb_peminjaman;

CREATE TABLE `tb_peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  CONSTRAINT `tb_peminjaman_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `tb_pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=latin1;

INSERT INTO tb_peminjaman VALUES("97","2019-03-30","2019-03-31","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("98","2019-04-01","2019-04-02","dikembalikan","5");
INSERT INTO tb_peminjaman VALUES("99","2019-04-01","2019-04-01","dikembalikan","5");
INSERT INTO tb_peminjaman VALUES("100","2019-04-02","2019-04-03","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("101","2019-04-02","2019-04-03","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("102","2019-04-02","2019-04-02","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("103","2019-04-02","2019-04-02","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("104","2019-04-02","2019-04-02","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("105","2019-04-02","2019-04-05","dipinjam","5");
INSERT INTO tb_peminjaman VALUES("106","2019-04-02","2019-04-10","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("107","2019-04-03","2019-04-03","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("108","2019-04-03","2019-04-10","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("109","2019-04-04","2019-04-05","dipinjam","5");
INSERT INTO tb_peminjaman VALUES("110","2019-04-04","2019-04-04","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("111","2019-04-04","2019-04-04","dikembalikan","5");
INSERT INTO tb_peminjaman VALUES("112","2019-04-05","2019-04-05","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("113","2019-04-05","2019-04-09","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("114","2019-04-05","2019-04-12","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("115","2019-04-06","2019-05-06","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("116","2019-04-06","2019-05-06","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("117","2019-04-05","2019-04-06","dipinjam","5");



DROP TABLE tb_petugas;

CREATE TABLE `tb_petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y Aktif',
  `nama_petugas` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `tb_petugas_ibfk_2` FOREIGN KEY (`id_level`) REFERENCES `tb_level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

INSERT INTO tb_petugas VALUES("38","admin","admin","Y","admin","frqn0101@gmail.com","3");
INSERT INTO tb_petugas VALUES("39","operator","operator","Y","operator","operator@operator","2");
INSERT INTO tb_petugas VALUES("40","peminjam","peminjam","Y","peminjam","peminjam@peminjam.com","1");



DROP TABLE tb_ruang;

CREATE TABLE `tb_ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(15) NOT NULL,
  `keter` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO tb_ruang VALUES("1","LAB RPL 1","R-001","RPL1");
INSERT INTO tb_ruang VALUES("2","LAB RPL 2","R-002","RPL2");
INSERT INTO tb_ruang VALUES("3","LAB RPL 3","R-003","RPL3");
INSERT INTO tb_ruang VALUES("6","Bengkel Pengelasan","R-004","Pengelasan");
INSERT INTO tb_ruang VALUES("7","Studio Animasii","R-005","Studio Animasii");
INSERT INTO tb_ruang VALUES("8"," R-Guru ","R-006","Tempat guru");
INSERT INTO tb_ruang VALUES("9","R-Perpustakaan","R-007","Perpustakaan");
INSERT INTO tb_ruang VALUES("10","R-Tata Usaha","R-008","TU");
INSERT INTO tb_ruang VALUES("11","XI-TKR1","R-009","TKR");



