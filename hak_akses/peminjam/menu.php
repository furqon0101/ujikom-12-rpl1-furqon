  <header class="main-header">
      <!-- Logo -->
      <a href="" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>I</b>NV</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>INVENTARIS</b></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>


        <div class="navbar-custom-menu">

      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
     <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/luser.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Peminjam</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </br>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Main Navigation</li>
        <li><a><i class="fa fa-calendar"></i><span><?php echo date("d-M-Y");?></span></a></li>
        <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Beranda</span></a></li>
        <li class="header">Lainnya</li>
            <?php
               include"koneksi.php";
               $id=$_SESSION['id_petugas'];
               date_default_timezone_set("Asia/Jakarta");
               $tanggal_pinjam=date('Y-m-d');
               $select=mysqli_query($konek, "SELECT * FROM tb_peminjaman WHERE id_pegawai='$id' AND tanggal_pinjam='$tanggal_pinjam' ORDER BY id_peminjaman DESC LIMIT 1");
                while($show=mysqli_fetch_array($select)){
            ?>
                        
        <li><a href="meminjam.php?id_peminjaman=<?php echo $show['id_peminjaman']; ?>"><i class="fa fa-cube"></i> <span>Tambah Pinjaman</span></a></li>
          <?php } ?>
        <li><a href="d_pinjaman.php"><i class="fa fa-cube"></i> <span>Data Pinjaman</span></a></li>
        <li><a href="d_inventaris.php"><i class="fa fa-cubes"></i> <span>Data Inventaris</span></a></li>
        <li><a href="logout.php"><i class="fa fa-sign-out"></i> <span>Keluar</span></a></li>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>