<?php
  session_start();
  if(!isset($_SESSION['username'])){
    echo '<script>document.location.href="index.php";</script>';
  }
?>
<?php
error_reporting(0);
if (isset($_SESSION['id_level']))
{
    // jika level admin
    if ($_SESSION['id_level'] == "3")
   {   
   }
   // jika kondisi level karyawan maka akan diarahkan ke halaman lain
   else if ($_SESSION['id_level'] == "2")
   {
       header('location:operator/index.php');
   }
}
if (!isset($_SESSION['id_level']))
{
    header('location:../../../index.php');
}
?>
<?php
include('modul/header.php');
?>


  <div class="content-wrapper">
    <section class="content-header">
      <h1>Minjam</h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
         <li class="active">Meminjam</li>
      </ol>
    </section>
    <!-- Main content -->
      <section class="content">
        <!-- Starts Widget -->
        <?php
          include('widget.php');
        ?>
        <!-- End Widget -->
        <div class="row">
          <!-- Start Formulir -->
          <div class="col-md-6">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-pencil"></i>
                <h3 class="box-title">Meminjam <small></small></h3>
                  <form role="form" action="prostamb_meminjam.php" method="post">
                    <!-- Box Body -->
                    <div class="box-body">
                      <p><i>*Minjamlah barang dengan bijak, rawat dan jagalah barang yang anda pinjam</i></p>
                      <div class="form-group">
                        <label>Nama Inventaris :</label>
                          <select name="id_inventaris" class="form-control" required>
                          <option>- Pilih Inventaris -</option>
                            <?php
                              include "konek.php";
                              $select=mysqli_query($konek, "SELECT * FROM tb_inventaris WHERE jumlah>0 ORDER BY nama DESC");
                              while($show=mysqli_fetch_array($select)){
                            ?>
                              <option value="<?=$show['id_inventaris'];?>"><?=$show['nama'];?></option>
                            <?php } ?>
                          </select>
                      </div>
                      <div class="form-group">
                        <label>Jumlah yang akan dipinjam :</label>
                        <input name="jumlahp" type="number" id="t1" class="form-control" placeholder="" 
                        onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
                        onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required="">
                      </div>     
                      <div class="form-group">
                        <label>ID Peminjaman :</label>
                        <?php
                            $id_peminjaman = $_GET['id_peminjaman'];
                            include "konek.php";
                            $select=mysqli_query($konek, "SELECT * FROM tb_peminjaman where id_peminjaman='$id_peminjaman' ");
                            while($show=mysqli_fetch_array($select)){
                        ?>
                        <input name="id_peminjaman" type="number" value="<?php echo $show['id_peminjaman'] ?>" class="form-control" required="" readonly>
                          <?php } ?>
                      </div>
                    </div>
                    <!-- End Box -->
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" value="simpan">Simpan</button>
                    </div>
                  </form>
                  <!-- End Form -->
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>

            </div>
          </div>
          <!-- End Formulir -->
          <div class="col-md-6" style="">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-barcode"></i>
                <h3 class="box-title">Data <small>Inventaris</small></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Inventaris</th>
                        <th>Spesifikasi</th>
                        <th>Jumlah</th>
                        <th>Kode Inventaris</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                          $no=1;
                          include "koneksi.php";
                          $query_mysqli = mysqli_query ($konek,"SELECT * FROM tb_inventaris WHERE jumlah>0 ORDER BY jumlah DESC") or die (mysqli_error());
                          while($data = mysqli_fetch_array($query_mysqli)){
                        ?>
                      <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $data['nama'] ?></td>
                        <td><?php echo $data['spesifikasi'] ?></td>
                        <td><?php echo $data['jumlah'] ?></td>
                        <td><?php echo $data['kode_inventaris'] ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
<?php
include('modul/footer.php');
?>


<script type="text/javascript" language=JavaScript>
function inputDigitsOnly(e) {
 var chrTyped, chrCode=0, evt=e?e:event;
 if (evt.charCode!=null)     chrCode = evt.charCode;
 else if (evt.which!=null)   chrCode = evt.which;
 else if (evt.keyCode!=null) chrCode = evt.keyCode;

 if (chrCode==0) chrTyped = 'SPECIAL KEY';
 else chrTyped = String.fromCharCode(chrCode);

 //[test only:] display chrTyped on the status bar 
 self.status='inputDigitsOnly: chrTyped = '+chrTyped;

 //Digits, special keys & backspace [\b] work as usual:
 if (chrTyped.match(/\d|[\b]|SPECIAL/)) return true;
 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;

 //Any other input? Prevent the default response:
 if (evt.preventDefault) evt.preventDefault();
 evt.returnValue=false;
 return false;
}

function addEventHandler(elem,eventType,handler) {
 if (elem.addEventListener) elem.addEventListener (eventType,handler,false);
 else if (elem.attachEvent) elem.attachEvent ('on'+eventType,handler); 
 else return 0;
 return 1;
}

// onload: Call the init() function to add event handlers!
function init() {
 addEventHandler(self.document.f2.elements[0],'keypress',inputDigitsOnly);
 addEventHandler(self.document.f2.elements[1],'keypress',inputDigitsOnly);
}

</script>