<?php
  session_start();
  if(!isset($_SESSION['username'])){
    echo '<script>document.location.href="index.php";</script>';
  }
?>
<?php
error_reporting(0);
if (isset($_SESSION['id_level']))
{
    // jika level admin
    if ($_SESSION['id_level'] == "3")
   {   
   }
   // jika kondisi level karyawan maka akan diarahkan ke halaman lain
   else if ($_SESSION['id_level'] == "2")
   {
       header('location:operator/index.php');
   }
}
if (!isset($_SESSION['id_level']))
{
    header('location:../../../index.php');
}
?>
<?php 
include 'header.php';
?>
<!DOCTYPE html>
<html>
<head>

    <link rel="stylesheet" href="css/bootstrap-material-datetimepicker.css" />
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>I</b>NV</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>INV</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../dist/img/download.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Administrator</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      <li class="header"><?php echo date("d-M-Y");?></li>
        <li><a href="../../index.php">
            <i class="fa fa-dashboard"></i> <span>Beranda</span>
            <span class="pull-right-container">
              <i class=""></i>
            </span>
        </a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="d_inventaris.php"><i class="fa fa-circle-o"></i> Data Inventaris</a></li>
            <li><a href="d_invenruang.php"><i class="fa fa-circle-o"></i> Data Inventaris Ruangan</a></li>
            <li><a href="d_detpin.php"><i class="fa fa-circle-o"></i> Data Peminjaman</a></li>
            <li><a href="d_petugas.php"><i class="fa fa-circle-o"></i> Data Petugas</a></li>
            <li><a href="d_pegawai.php"><i class="fa fa-circle-o"></i> Data Pegawai</a></li>
          </ul>
        </li>
        <li class="treeview">
        <li><a href="minjam.php"><i class="fa fa-cube"></i> <span>Minjam Barang</span><span class="pull-right-container"></span>
        </a></li>
        <li class="header">--------------------------------------------------</li>
        <li><a href="backup.php"><i class="fa fa-database"></i> <span>Back Up Database</span></a></li>
        <li><a href="logout.php"><i class="fa fa-sign-out"></i> <span>Keluar</span></a></li>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>Minjam</h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
         <li class="active">Minjam</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-pencil"></i>
                <h3 class="box-title">Meminjam <small>Inventaris</small></h3>
                  <form role="form" action="prostamb_pinjam.php" method="post">
                    <!-- Box Body -->
                    <div class="box-body">
                      <p><i>*Untuk meminjam barang silahkan tentukan tanggal kembali terlebih dulu untuk mendapatkan ID peminjaman anda</i></p>
                      <div class="form-group">
                        <label>ID User :</label>
                        <select name="id_pegawai" class="form-control" required="">
                        <option>-</option>
                          <?php
                            include "konek.php";
                            $select=mysqli_query($konek, "SELECT * FROM tb_pegawai");
                            while($show=mysqli_fetch_array($select)){
                          ?>
                            <option value="<?=$show['id_pegawai'];?>"><?=$show['id_pegawai'];?>. <?=$show['nama_pegawai'];?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Tanggal Pinjam :</label>
                        <input name="tanggal_pinjam" type="date" class="form-control" value="<?php echo date("Y-m-d");?>" readonly="">
                      </div>
                      <div class="form-group">
                        <label>Tanggal Kembali :</label>
                        <input name="tanggal_kembali" type="text" id="min-date" class="form-control" placeholder="" required>
                      </div>
                    </div>
                    <!-- End Box -->
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" value="simpan">Simpan</button>
                    </div>
                  </form>
                  <!-- End Form -->
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>

            </div>
          </div>
          <!-- End Formulir -->
          <div class="col-md-6" style="">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-barcode"></i>
                <h3 class="box-title">Data <small>Inventaris</small></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Inventaris</th>
                        <th>Spesifikasi</th>
                        <th>Jumlah</th>
                        <th>Kode Inventaris</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                          $no=1;
                          include "konek.php";
                          $query_mysqli = mysqli_query ($konek,"SELECT * FROM tb_inventaris WHERE jumlah>0 ORDER BY jumlah DESC") or die (mysqli_error());
                          while($data = mysqli_fetch_array($query_mysqli)){
                        ?>
                      <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $data['nama'] ?></td>
                        <td><?php echo $data['spesifikasi'] ?></td>
                        <td><?php echo $data['jumlah'] ?></td>
                        <td><?php echo $data['kode_inventaris'] ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
</div>
</section>
</div>


<?php
include('modul/footer.php');
?>
<script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
    <script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
    <script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="js/bootstrap-material-datetimepicker.js"></script>

     <script type="text/javascript">
    $(document).ready(function()
    {
      $('#date').bootstrapMaterialDatePicker
      ({
        time: false,
        clearButton: true
      });

      $('#time').bootstrapMaterialDatePicker
      ({
        date: false,
        shortTime: false,
      });

      $('#date-format').bootstrapMaterialDatePicker
      ({
        format: 'dddd DD MMMM YYYY'
      });
      $('#date-fr').bootstrapMaterialDatePicker
      ({
        format: 'DD/MM/YYYY',
        lang: 'fr',
        weekStart: 1, 
        cancelText : 'ANNULER',
        nowButton : true,
        switchOnClick : true
      });

      $('#date-end').bootstrapMaterialDatePicker
      ({
        weekStart: 0, format: 'DD/MM/YYYY'
      });
      $('#date-start').bootstrapMaterialDatePicker
      ({
        weekStart: 0, format: 'DD/MM/YYYY', shortTime : true
      }).on('change', function(e, date)
      {
        $('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
      });

      $('#min-date').bootstrapMaterialDatePicker({ format : 'YYYY/MM/DD ', time: false,  lang: 'id', minDate : new Date() });

      $.material.init()
    });
    </script>