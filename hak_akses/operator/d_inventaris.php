<?php
session_start();
if(!isset($_SESSION['username'])){
    die("<script>alert('Silahkan login terlebih dahulu!');document.location.href='../index.php'</script>");//
}
?>
<?php
    include('link.php');
?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php
      include('menu.php');
    ?>
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Beranda
          <small>Halaman Peminjam</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="index"><i class="fa fa-home"></i> Beranda</a></li>
          <li><a href="produk"><i class="fa fa-barcode"></i> Data Inventaris</a></li>
        </ol>
      </section>
      <section class="content">
        <!-- Starts Widget -->
        <?php
          include('widget.php');
        ?>
        <!-- End Widget -->
        <div class="row">

          <div class="col-md-12" style="">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-cubes"></i>
                <h3 class="box-title">Data <small>Inventaris</small></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Spesifikasi</th>
                        <th>Kondisi</th>
                        <th>Keterangan</th>
                        <th>Jumlah</th>
                        <th>Jenis</th>
                        <th>Tanggal Masuk</th>
                        <th>Ruangan</th>
                        <th>Kode Inventaris</th>
                        <th>Sumber</th>
                        <th>Nama Petugas</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                          $no=1;
                          include "koneksi.php";
                          $query_mysqli = mysqli_query ($konek,"SELECT * FROM tb_inventaris INNER JOIN tb_jenis on tb_inventaris.id_jenis = tb_jenis.id_jenis 
                            INNER JOIN tb_ruang on tb_inventaris.id_ruang = tb_ruang.id_ruang INNER JOIN tb_petugas on tb_inventaris.id_petugas = tb_petugas.id_petugas 
                            ORDER BY id_inventaris DESC") or die (mysqli_error());
                          while($data = mysqli_fetch_array($query_mysqli)){
                        ?>
                      <tr>
                        <td><?php echo $no++ ?></td>
                        <td class="text-center"><?php echo $data['nama']; ?></td>
                        <td class="text-center"><?php echo $data['spesifikasi']; ?></td>
                        <td class="text-center"><?php echo $data['kondisi']; ?></td>
                        <td class="text-center"><?php echo $data['keterangan']; ?></td>
                        <td class="text-center"><?php echo $data['jumlah']; ?></td>
                        <td class="text-center"><?php echo $data['nama_jenis']; ?></td>
                        <td class="text-center"><?php echo date ('d F Y', strtotime($data['tanggal_register'])) ?></td>
                        <td class="text-center"><?php echo $data['nama_ruang']; ?></td>
                        <td class="text-center"><?php echo $data['kode_inventaris']; ?></td>
                        <td class="text-center"><?php echo $data['sumber']; ?></td>
                        <td class="text-center"><?php echo $data['nama_petugas']; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <?php
      include('sidebar.php');
    ?>