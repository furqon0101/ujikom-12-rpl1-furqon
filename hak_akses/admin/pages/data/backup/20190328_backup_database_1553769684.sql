DROP TABLE tb_detail_pinjam;

CREATE TABLE `tb_detail_pinjam` (
  `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `jumlahp` varchar(50) NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  PRIMARY KEY (`id_detail_pinjam`),
  KEY `id_inventaris` (`id_inventaris`),
  KEY `id_peminjaman` (`id_peminjaman`),
  CONSTRAINT `tb_detail_pinjam_ibfk_2` FOREIGN KEY (`id_peminjaman`) REFERENCES `tb_peminjaman` (`id_peminjaman`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_detail_pinjam_ibfk_3` FOREIGN KEY (`id_inventaris`) REFERENCES `tb_inventaris` (`id_inventaris`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

INSERT INTO tb_detail_pinjam VALUES("91","53","5","dikembalikan","49");
INSERT INTO tb_detail_pinjam VALUES("92","52","10","dikembalikan","49");
INSERT INTO tb_detail_pinjam VALUES("93","53","5","dikembalikan","50");
INSERT INTO tb_detail_pinjam VALUES("94","52","10","dikembalikan","50");
INSERT INTO tb_detail_pinjam VALUES("95","53","2","dikembalikan","52");
INSERT INTO tb_detail_pinjam VALUES("96","53","1","dikembalikan","54");
INSERT INTO tb_detail_pinjam VALUES("97","53","1","dikembalikan","58");
INSERT INTO tb_detail_pinjam VALUES("98","52","2","dikembalikan","58");
INSERT INTO tb_detail_pinjam VALUES("99","54","3","dikembalikan","58");
INSERT INTO tb_detail_pinjam VALUES("100","53","7","dikembalikan","59");
INSERT INTO tb_detail_pinjam VALUES("101","53","1","dikembalikan","60");
INSERT INTO tb_detail_pinjam VALUES("102","53","1","dipinjam","61");
INSERT INTO tb_detail_pinjam VALUES("103","52","7","dikembalikan","71");
INSERT INTO tb_detail_pinjam VALUES("104","52","7","dipinjam","73");
INSERT INTO tb_detail_pinjam VALUES("105","52","10","dipinjam","80");
INSERT INTO tb_detail_pinjam VALUES("106","54","10","dipinjam","82");



DROP TABLE tb_inventaris;

CREATE TABLE `tb_inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `spesifikasi` varchar(60) NOT NULL,
  `kondisi` enum('baik','rusak') NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` varchar(50) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(15) NOT NULL,
  `sumber` varchar(60) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`),
  KEY `id_ruang` (`id_ruang`),
  KEY `id_petugas` (`id_petugas`),
  CONSTRAINT `tb_inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `tb_jenis` (`id_jenis`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `tb_ruang` (`id_ruang`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `tb_petugas` (`id_petugas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

INSERT INTO tb_inventaris VALUES("52","Laptop","AMD","baik","Kondisi Bagus","170","1","2019-02-26","1","IS0003","Pemerintah","38");
INSERT INTO tb_inventaris VALUES("53","Meja","Furniture","rusak","Bejad satu coy","16","3","2019-02-27","3","IS0002","Pemerintah","38");
INSERT INTO tb_inventaris VALUES("54","Bangku","Samsung","baik","Tersedia","140","1","2019-03-08","1","IS0001","Pemerintah","38");



DROP TABLE tb_jenis;

CREATE TABLE `tb_jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(15) NOT NULL,
  `ket` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO tb_jenis VALUES("1","Elekronik","JNS001","Elektronik");
INSERT INTO tb_jenis VALUES("2","Kelas","JNS002","Kelas");
INSERT INTO tb_jenis VALUES("3","Furniture","JNS003","Furniture");



DROP TABLE tb_level;

CREATE TABLE `tb_level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(50) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO tb_level VALUES("1","Peminjam");
INSERT INTO tb_level VALUES("2","Operator");
INSERT INTO tb_level VALUES("3","Admin");



DROP TABLE tb_pegawai;

CREATE TABLE `tb_pegawai` (
  `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_pegawai` varchar(50) NOT NULL,
  `nip` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  PRIMARY KEY (`id_pegawai`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO tb_pegawai VALUES("1","peg1","peg1","pegawai","01122","ciomas");
INSERT INTO tb_pegawai VALUES("2","pegawai2","pegawai2","pegawai 2","0192312","pagelaran");
INSERT INTO tb_pegawai VALUES("3","pegawai","pegawai","pegawai 3","01298126","Laladon");
INSERT INTO tb_pegawai VALUES("4","FURQON","FURQON","FURQON","123123123","Villa Ciomas");



DROP TABLE tb_peminjaman;

CREATE TABLE `tb_peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_pegawai` (`id_pegawai`),
  CONSTRAINT `tb_peminjaman_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `tb_pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;

INSERT INTO tb_peminjaman VALUES("49","2019-03-08","2019-03-09","dikembalikan","1");
INSERT INTO tb_peminjaman VALUES("50","2019-03-08","2019-03-10","dikembalikan","1");
INSERT INTO tb_peminjaman VALUES("51","2019-03-08","2019-03-09","dipinjam","1");
INSERT INTO tb_peminjaman VALUES("52","2019-03-15","2019-03-15","dikembalikan","1");
INSERT INTO tb_peminjaman VALUES("54","2019-03-15","2019-03-15","dikembalikan","1");
INSERT INTO tb_peminjaman VALUES("58","2019-03-14","2019-03-14","dikembalikan","2");
INSERT INTO tb_peminjaman VALUES("59","2019-03-14","2019-03-01","dikembalikan","2");
INSERT INTO tb_peminjaman VALUES("60","2019-03-17","2019-03-18","dikembalikan","2");
INSERT INTO tb_peminjaman VALUES("61","2019-03-17","2019-03-18","dipinjam","1");
INSERT INTO tb_peminjaman VALUES("71","2019-03-17","2019-03-17","dikembalikan","4");
INSERT INTO tb_peminjaman VALUES("73","2019-03-17","2019-03-01","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("74","2019-03-17","2019-03-17","dipinjam","2");
INSERT INTO tb_peminjaman VALUES("75","2019-03-17","2019-03-02","dipinjam","3");
INSERT INTO tb_peminjaman VALUES("80","2019-03-19","2019-03-26","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("81","2019-03-19","2019-03-20","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("82","2019-03-19","2019-03-20","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("83","2019-03-19","2019-03-14","dipinjam","4");
INSERT INTO tb_peminjaman VALUES("84","0000-00-00","2019-03-20","dipinjam","1");
INSERT INTO tb_peminjaman VALUES("85","0000-00-00","2019-04-04","dipinjam","2");
INSERT INTO tb_peminjaman VALUES("86","0000-00-00","2019-03-21","dipinjam","1");
INSERT INTO tb_peminjaman VALUES("87","2019-03-19","2019-03-20","dipinjam","1");



DROP TABLE tb_petugas;

CREATE TABLE `tb_petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT 'N' COMMENT 'N Tidak Aktif, Y Aktif',
  `nama_petugas` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`),
  CONSTRAINT `tb_petugas_ibfk_2` FOREIGN KEY (`id_level`) REFERENCES `tb_level` (`id_level`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

INSERT INTO tb_petugas VALUES("38","admin","admin","Y","admin","frqn0101@gmail.com","3");
INSERT INTO tb_petugas VALUES("39","operator","operator","Y","operator","operator@operator","2");



DROP TABLE tb_ruang;

CREATE TABLE `tb_ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(15) NOT NULL,
  `keter` text NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO tb_ruang VALUES("1","LAB RPL 1","R-001","RPL1");
INSERT INTO tb_ruang VALUES("2","LAB RPL 2","R-002","RPL2");
INSERT INTO tb_ruang VALUES("3","LAB RPL 3","R-003","RPL3");
INSERT INTO tb_ruang VALUES("6","Bengkel Pengelasan","R-004","Pengelasan");
INSERT INTO tb_ruang VALUES("7","Studio Animasii","R-005","Studio Animasii");



