<?php
  session_start();
  if(!isset($_SESSION['username'])){
    echo '<script>document.location.href="index.php";</script>';
  }
?>
<?php
error_reporting(0);
if (isset($_SESSION['id_level']))
{
    // jika level admin
    if ($_SESSION['id_level'] == "3")
   {   
   }
   // jika kondisi level karyawan maka akan diarahkan ke halaman lain
   else if ($_SESSION['id_level'] == "2")
   {
       header('location:operator/index.php');
   }
}
if (!isset($_SESSION['id_level']))
{
    header('location:index.php');
}
?>
 <?php
include('modul/header.php');
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Beranda
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
								<?php
									include "konek.php";
									$jumlah = 0;
									$sql = mysqli_query($konek, "SELECT nama_petugas FROM tb_petugas");
									while($row = mysqli_fetch_array($sql)){
										$jumlah++;
									}
									echo "<h3> $jumlah </h3>"
								?>
              <p>Jumlah Petugas</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="pages/data/d_petugas.php" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
								<?php
									include "konek.php";
									$jumlah = 0;
									$sql = mysqli_query($konek, "SELECT nama FROM tb_inventaris");
									while($row = mysqli_fetch_array($sql)){
										$jumlah++;
									}
									echo "<h3> $jumlah </h3>"
								?>
              <p>Data Inventaris</p>
            </div>
            <div class="icon">
              <i class="fa fa-cubes"></i>
            </div>
            <a href="pages/data/d_inventaris.php" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
								<?php
									include "konek.php";
									$jumlah = 0;
									$sql = mysqli_query($konek, "SELECT nama_jenis FROM tb_jenis");
									while($row = mysqli_fetch_array($sql)){
										$jumlah++;
									}
									echo "<h3> $jumlah </h3>"
								?>
              <p>Data Jenis</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-text"></i>
            </div>
            <a href="pages/data/d_jenis.php" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
								<?php
									include "konek.php";
									$jumlah = 0;
									$sql = mysqli_query($konek, "SELECT nama_ruang FROM tb_ruang");
									while($row = mysqli_fetch_array($sql)){
										$jumlah++;
									}
									echo "<h3> $jumlah </h3>"
								?>
              <p>Data Ruangan</p>
            </div>
            <div class="icon">
              <i class="fa fa-save (alias)"></i>
            </div>
            <a href="pages/data/d_ruang.php" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
    <div class="row">
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Petugas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Username</th>
                  <th>Nama Petugas</th>
                  <th>Email</th>
                </tr>
					<?php
						include "konek.php";
						$query_mysql = mysqli_query ($konek, "SELECT * FROM tb_petugas LIMIT 3") or die (mysqli_error());
						$i = 1;
						while($data = mysqli_fetch_array($query_mysql)){
					?>
						<tr>
							<td class="text-center"><?php echo $i++;?></td>
							<td><?php echo $data['username']; ?></td>
							<td><?php echo $data['nama_petugas']; ?></td>
							<td><?php echo $data['email']; ?></td>
						</tr>
					<?php
						}
					?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
			<a href="pages/data/d_petugas.php"><button type="button" class="btn btn-block btn-default">Selengkapnya...</button><a/>
            </div>
          </div>
        </div>
		
		
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Inventaris</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Nama Inventaris</th>
                  <th>Kondisi</th>
                  <th>Jumlah</th>
                </tr>
					<?php
						include "konek.php";
						$query_mysql = mysqli_query ($konek, "SELECT * FROM tb_inventaris LIMIT 3") or die (mysqli_error());
						$i = 1;
						while($data = mysqli_fetch_array($query_mysql)){
					?>
						<tr>
							<td class="text-center"><?php echo $i++;?></td>
							<td><?php echo $data['nama']; ?></td>
							<td><?php echo $data['kondisi']; ?></td>
							<td><?php echo $data['jumlah']; ?></td>
						</tr>
					<?php
						}
					?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
			<a href="pages/data/d_inventaris.php"><button type="button" class="btn btn-block btn-default">Selengkapnya...</button><a/>
            </div>
          </div>
        </div>
		
		
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Jenis</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Nama Jenis</th>
                  <th>Kode Jenis</th>
                  <th>Keterangan</th>
                </tr>
					<?php
						include "konek.php";
						$query_mysql = mysqli_query ($konek, "SELECT * FROM tb_jenis LIMIT 3") or die (mysqli_error());
						$i = 1;
						while($data = mysqli_fetch_array($query_mysql)){
					?>
						<tr>
							<td class="text-center"><?php echo $i++;?></td>
							<td><?php echo $data['nama_jenis']; ?></td>
							<td><?php echo $data['kode_jenis']; ?></td>
							<td><?php echo $data['ket']; ?></td>
						</tr>
					<?php
						}
					?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
			       <a href="pages/data/d_jenis.php"><button type="button" class="btn btn-block btn-default">Selengkapnya...</button><a/>
            </div>
          </div>
        </div>
		
		
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Ruangan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">No</th>
                  <th>Ruangan</th>
                  <th>Kode Ruang</th>
                  <th>Keterangan</th>
                </tr>
					<?php
						include "konek.php";
						$query_mysql = mysqli_query ($konek, "SELECT * FROM tb_ruang LIMIT 3") or die (mysqli_error());
						$i = 1;
						while($data = mysqli_fetch_array($query_mysql)){
					?>
						<tr>
							<td class="text-center"><?php echo $i++;?></td>
							<td><?php echo $data['nama_ruang']; ?></td>
							<td><?php echo $data['kode_ruang']; ?></td>
							<td><?php echo $data['keter']; ?></td>
						</tr>
					<?php
						}
					?>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
			<a href="pages/data/d_ruang.php"><button type="button" class="btn btn-block btn-default">Selengkapnya...</button></a>
            </div>
          </div>
        </div>
                <!-- /.col -->
      </div>
    </section>

    </section>
        <!-- right col -->
      </div>
  </div>
  
<?php
include('modul/footer.php');
?>