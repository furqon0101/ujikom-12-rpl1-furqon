<?php
session_start();
if(!isset($_SESSION['username'])){
    die("<script>alert('Silahkan login terlebih dahulu!');document.location.href='../index.php'</script>");//
}
?>
<?php
    include('link.php');
?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php
      include('menu.php');
    ?>
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Beranda
          <small>Halaman Peminjam</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="index"><i class="fa fa-home"></i> Beranda</a></li>
          <li><a href="produk"><i class="fa fa-barcode"></i> Data Inventaris</a></li>
        </ol>
      </section>
      <section class="content">
        <!-- Starts Widget -->
        <?php
          include('widget.php');
        ?>
        <!-- End Widget -->
        <div class="row">

          <div class="col-md-12" style="">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-barcode"></i>
                <h3 class="box-title">Data <small>Inventaris</small></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>ID Peminjaman</th>
                        <th>Nama Inventaris</th>
                        <th>Jumlah</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                          $no=1;
                          include "koneksi.php";
                          $id=$_GET['id_peminjaman'];
                          $query_mysql = mysqli_query ($konek, "SELECT * FROM tb_detail_pinjam INNER JOIN tb_inventaris on tb_detail_pinjam.id_inventaris = tb_inventaris.id_inventaris WHERE id_peminjaman='$id' ORDER BY id_detail_pinjam DESC") or die (mysqli_error());
                            $i = 1;
                            while($data = mysqli_fetch_array($query_mysql)){
                        ?>
                            <tr>
                                <td><?php echo $i++;?></td>
                                <td><?php echo $data['id_peminjaman']; ?></td>
                                <td><?php echo $data['nama']; ?></td>
                                <td><?php echo $data['jumlahp']; ?></td>
                                <td><?php echo $data['status_peminjaman']; ?></td>
                                </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <?php
      include('sidebar.php');
    ?>