<?php
session_start();
if(!isset($_SESSION['username'])){
    die("<script>alert('Silahkan login terlebih dahulu!');document.location.href='../index.php'</script>");//
}
?>
<?php
    include('link.php');
?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php
      include('menu.php');
    ?>
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Beranda
          <small>Halaman Peminjam</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href=""><i class="fa fa-home"></i> Beranda</a></li>
          <li><a href=""><i class=""></i> Minjam</a></li>
        </ol>
      </section>
      <section class="content">
        <!-- Starts Widget -->
        <?php
          include('widget.php');
        ?>
        <!-- End Widget -->
        <div class="row">
          <!-- Start Formulir -->
          <div class="col-md-6">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-pencil"></i>
                <h3 class="box-title">Meminjam <small>Inventaris</small></h3>
                  <form role="form" action="prostamb_minjam.php" method="post">
                    <!-- Box Body -->
                    <div class="box-body">
                      <p><i>*Untuk meminjam barang silahkan tentukan tanggal kembali terlebih dulu untuk mendapatkan ID peminjaman anda</i></p>
                      <p><i>*Pastikan barang yang akan anda pinjam tersedia! Silahkan cek di bagian data inventaris.</i></p>
                      <p><i>*Gunakanlah ID sesuai dengan yang anda miliki.</i></p>
                      <div class="form-group">
                        <label>ID User :</label>
                        <select name="id_pegawai" class="form-control">
                        <option>-</option>
                          <?php
                            include "koneksi.php";
                            $select=mysqli_query($konek, "SELECT * FROM tb_pegawai");
                            while($show=mysqli_fetch_array($select)){
                          ?>
                            <option value="<?=$show['id_pegawai'];?>"><?=$show['id_pegawai'];?>. <?=$show['nama_pegawai'];?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Tanggal Pinjam :</label>
                        <input name="tanggal_pinjam" type="date" class="form-control" value="<?php echo date("Y-m-d");?>" readonly="">
                      </div>
                      <div class="form-group">
                        <label>Tanggal Kembali :</label>
                        <input name="tanggal_kembali" type="text" id="min-date" class="form-control" required="">
                      </div>
                    </div>
                    <!-- End Box -->
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" value="simpan">Simpan</button>
                    </div>
                  </form>
                  <!-- End Form -->
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                </div>

            </div>
          </div>
          <!-- End Formulir -->
          <div class="col-md-6" style="">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-barcode"></i>
                <h3 class="box-title">Data <small>Inventaris</small></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="table-responsive">
                 <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama Inventaris</th>
                        <th>Kondisi</th>
                        <th>Jumlah</th>
                        <th>Kode Inventaris</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                          $no=1;
                          include "koneksi.php";
                          $query_mysqli = mysqli_query ($konek,"SELECT * FROM tb_inventaris where jumlah>0 ORDER BY jumlah desc") or die (mysqli_error());
                          while($data = mysqli_fetch_array($query_mysqli)){
                        ?>
                      <tr>
                        <td><?php echo $no++ ?></td>
                        <td><?php echo $data['nama'] ?></td>
                        <td><?php echo $data['kondisi'] ?></td>
                        <td><?php echo $data['jumlah'] ?></td>
                        <td><?php echo $data['kode_inventaris'] ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <?php
      include('sidebar.php');
    ?>