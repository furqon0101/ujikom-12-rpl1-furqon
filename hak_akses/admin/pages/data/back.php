<?php
	include ('modul/header.php');
?>


  <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Data Inventaris Ruangan</h1>
			<ol class="breadcrumb">
			   <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
			   <li class="active">Data Petugas</li>
			</ol>
		</section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
						<label class="text-center"> Periksa Ruangan </label>
						<select name="id_ruang" id="ruang-change"  class="form-control">
						<option>Pilih Ruangan Yang Akan di Cek Data Inventaris Ruangannya!</option>
							<?php     
								include"konek.php";
								$select=mysqli_query($konek,"SELECT * FROM tb_ruang");
								while($show=mysqli_fetch_array($select)){
							?>
								<option value="<?=$show['id_ruang'];?>"><?=$show['nama_ruang'];?></option>
							<?php } ?>
						</select>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
	           	<?php 
	            include "konek.php";
	            error_reporting(0);
	           	if ($_GET['id_ruang']>0) {
	           		?>
	           		<div class="table-responsive">
	              <table id="example1" class="table table-bordered table-striped">
	                <thead>
						<tr>
						  <th class="text-center" style="margin:10px">No</th>
						  <th class="text-center">Ruangan</th>
						  <th class="text-center">Nama Barang</th>
						  <th class="text-center">Kode Inventaris</th>
						  <th class="text-center">Jumlah</th>
						</tr>
	                </thead>
					
	                <tbody>
	                <?php
						if(isset($_GET['id_ruang'])){
						$id = $_GET['id_ruang'];
										}
	                    $query_mysql = mysqli_query ($konek, "SELECT * FROM tb_inventaris  join tb_ruang on tb_inventaris.id_ruang = tb_ruang.id_ruang where tb_inventaris.id_ruang='$id' ")
						or die (mysqli_error());
	                    $i = 1;
	                    while($data = mysqli_fetch_array($query_mysql)){

	                ?>
	                                <tr>
	                                    <td class="text-center"><?php echo $i++;?></td>
	                                    <td class="text-center"><?php echo $data['nama_ruang']; ?></td>
	                                    <td class="text-center"><?php echo $data['nama']; ?></td>
	                                    <td class="text-center"><?php echo $data['kode_inventaris']; ?></td>
	                                    <td class="text-center"><?php echo $data['jumlah']; ?></td>
	                                </tr>
									
	                                <?php
	                                    }
	                                ?>
	                </tbody>
	              </table>
	              </div>
	              </div>
				</div>
				<?php
	           	}
	           	else{
	           		?>
	           		<h3 align="center">Teangan Weh di web pokep</h3>
	           	<?php
	           	}
	           	?>
	            
          </div>
          </div>
		</div>
    </section>
  </div>

<?php
include('modul/footer.php');
?>

<script>
			$(function(){
				$("#ruang-change").on("change",function(){
					window.location.href= "d_invenruang.php?id_ruang="+$(this).val();
				});
			});
</script>
