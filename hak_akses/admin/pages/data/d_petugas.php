<?php
  session_start();
  if(!isset($_SESSION['username'])){
    echo '<script>document.location.href="index";</script>';
  }
?>
<?php
error_reporting(0);
if (isset($_SESSION['id_level']))
{
    // jika level admin
    if ($_SESSION['id_level'] == "3")
   {   
   }
   // jika kondisi level karyawan maka akan diarahkan ke halaman lain
   else if ($_SESSION['id_level'] == "2")
   {
       header('location:operator/index.php');
   }
}
if (!isset($_SESSION['id_level']))
{
    header('location:../../../index.php');
}
?>
<?php
	include ('modul/header.php');
?>

  <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Data Petugas</h1>
			<ol class="breadcrumb">
			   <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
			   <li class="active">Data Petugas</li>
			</ol>
		</section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default">+Tambah Data</button>
              <button class="btn btn-info" data-toggle="modal" data-target="#modal-info">Print PDF</button>
		        <div class="modal modal-info fade" id="modal-info">
		          <div class="modal-dialog">
		            <div class="modal-content">
		              <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  <span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">INFORMASI</h4>
		              </div>
		              <div class="modal-body">
		                <p>File ini akan di download, untuk melihat silahkan ke folder download anda! Lanjutkan?&hellip;</p>
		              </div>
		              <div class="modal-footer">
		                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		                <a href="print_petugas.php"><button type="button" class="btn btn-outline">Lanjutkan..</button></a>
		              </div>
		            </div>
		          </div>
		        </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
					<tr>
					  <th class="text-center" style="margin:10px">No</th>
					  <th class="text-center">Username</th>
					  <th class="text-center">Password</th>
					  <th class="text-center">Status</th>
					  <th class="text-center">Nama Petugas</th>
					  <th class="text-center">Email</th>
					  <th class="text-center">ID level</th>
					  <th class="text-center">Opsi</th>
					</tr>
                </thead>
				
                <tbody>
                <?php
                    include "konek.php";
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM tb_petugas ptg INNER JOIN tb_level lvl on ptg.id_level=lvl.id_level ORDER BY id_petugas DESC") or die (mysqli_error());
                    $i = 1;
                    while($data = mysqli_fetch_array($query_mysql)){
                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['username']; ?></td>
                                    <td class="text-center"><?php echo $data['password']; ?></td>
                                    <td><?php
											if($data['status'] == 'Y')
											{
												?>
											<a href="approve.php?table=tb_petugas&id_petugas=<?php echo $data['id_petugas']; ?>&action=not-verifed"><font color="blue">
											Aktif</font>
											</a>
											<?php
											}else{
												?>
												
											<a href="approve.php?table=tb_petugas&id_petugas=<?php echo $data['id_petugas']; ?>&action=verifed"><font color="red">
											Tidak Aktif</font>
											</a>
											<?php
											}
											?>
										</td>
                                    <td class="text-center"><?php echo $data['nama_petugas']; ?></td>
                                    <td class="text-center"><?php echo $data['email']; ?></td>
                                    <td class="text-center"><?php echo $data['nama_level']; ?></td>
									<td class="text-center">
                                        <a href="#" data-toggle="modal" data-target="#modal-edit<?php echo $data['id_petugas'];?>" class="btn btn-success">Ubah</a>
                                     </td>
                                </tr>
								
			<div class="modal fade" id="modal-edit<?php echo $data['id_petugas'];?>">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Petugas</h4>
					  <div class="modal-body">
						<form method="POST" action="u_petugas.php">
						  <div class="box-body">
							<input type="hidden" name="id_petugas" value="<?php echo $data['id_petugas'] ?>" required>
							<div class="form-group">
							  <label>Username:</label>
							  <input name="username" type="text" class="form-control" placeholder="" value="<?php echo $data['username'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Password :</label>
							  <input name="password" type="text" class="form-control" placeholder="" value="<?php echo $data['password'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Nama Petugas :</label>
							  <input name="nama_petugas" type="text" class="form-control" placeholder="" value="<?php echo $data['nama_petugas'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Email :</label>
							  <input name="email" type="email" class="form-control" placeholder="" value="<?php echo $data['email'] ?>" required>
							</div>
							<div class="form-group">
							  <label>ID Level :  </label>
								<select name="id_level" class="form-control" required>
								  <option>-</option>
								  <option value="1" <?=$data['id_level']=='1'?'selected':null;?>>Peminjam</option>
								  <option value="2" <?=$data['id_level']=='2'?'selected':null;?>>Operator</option>
								  <option value="3" <?=$data['id_level']=='3'?'selected':null;?>>Admin</option>
								</select>
							</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary" value="update">Simpan Perubahan</button>
						  </div>
						</form>
					  </div>
				    </div>
				  </div>
				</div>
                                <?php
                                    }
                                ?>
                </tbody>
              </table>
			</div>
          </div>
			<div class="modal fade" id="modal-default">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Petugas</h4>
				  </div>
				  <div class="modal-body">			  
					<form method="POST" action="prostamb_petugas.php">
					  <div class="box-body">
						<div class="form-group">
						  <label>Username:</label>
						  <input name="username" type="text" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Password :</label>
						  <input name="password" type="text" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Nama Petugas :</label>
						  <input name="nama_petugas" type="text" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Email :</label>
						  <input name="email" type="email" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>ID Level :  </label>
							<select name="id_level" class="form-control" required>
								<?php     
									include"konek.php";
									$select=mysqli_query($konek, "SELECT * FROM tb_level");
									while($show=mysqli_fetch_array($select)){
								?>
									<option value="<?=$show['id_level'];?>"><?=$show['nama_level'];?></option>
								<?php } ?>
							</select>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary" value="Simpan">Simpan</button>
					  </div>
					</form>
				  </div>
				</div>
			  </div>
			</div>
          </div>
		</div>
    </section>
  </div>

<?php
include('modul/footer.php');
?>