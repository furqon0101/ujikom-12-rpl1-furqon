<div class="row">
          <!-- Detail Pengembalian -->
          <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-yellow">
              <div class="inner">
                <?php
                  include "koneksi.php";
                  $today = mysqli_num_rows(mysqli_query($konek, "SELECT * FROM tb_inventaris"));
                ?>
                <h3><?=0+$today?></h3>
                <p>Data Inventaris</p>
              </div>
              <div class="icon">
                <i class="fa fa-cubes"></i>
              </div>
              <a href="d_inventaris.php" class="small-box-footer">Info lebih lanjut <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- End Pengembalian -->
          <!-- Detail Peminjaman -->
          <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-red">
              <div class="inner">
                <h4>+</h4>
                <p>Data Pinjaman</p>
              </div>
              <div class="icon">
                <i class="fa fa-pencil "></i>
              </div>
              <a href="d_pinjaman.php" class="small-box-footer">Info lebih lanjut <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- End Peminjaman -->
        </div>