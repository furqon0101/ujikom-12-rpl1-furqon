<?php
  session_start();
  if(!isset($_SESSION['username'])){
    echo '<script>document.location.href="index";</script>';
  }
?>
<?php
error_reporting(0);
if (isset($_SESSION['id_level']))
{
    // jika level admin
    if ($_SESSION['id_level'] == "3")
   {   
   }
   // jika kondisi level karyawan maka akan diarahkan ke halaman lain
   else if ($_SESSION['id_level'] == "2")
   {
       header('location:operator/index.php');
   }
}
if (!isset($_SESSION['id_level']))
{
    header('location:../../../index.php');
}
?>
<?php
	include ('modul/header.php');
?>

  <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Data Peminjam</h1>
			<ol class="breadcrumb">
			   <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
			   <li class="active">Data Peminjam</li>
			</ol>
		</section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">

                <?php
                    include "konek.php";
                    $id=$_GET['id_peminjaman'];
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM tb_peminjaman WHERE id_peminjaman=$id ") or die (mysqli_error());
                    while($data = mysqli_fetch_array($query_mysql)){
                    ?>
			    <form action="prosub_status.php" method="post">
			        <div class="form-group" hidden="">
			            <input name="id_peminjaman" type="text" class="form-control" placeholder="" value="<?php echo $data['id_peminjaman'] ?>" required>
			        </div>
			        <?php if($data ['status_peminjaman'] == "dipinjam") {?>
			      <input type="submit" class="btn btn-success" value="<?php echo $data['status_peminjaman'] ?>">
			      <?php } elseif($data ['status_peminjaman'] == "dikembalikan") {?>
			      <input type="button" class="btn btn-danger" value="<?php echo $data['status_peminjaman'] ?>">
			    <?php } ?>
			        
			    </form>
			    <?php } ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
					<tr>
					  <th class="text-center" style="margin:10px">No</th>
					  <th class="text-center">ID Peminjaman</th>
					  <th class="text-center">Nama Barang</th>
					  <th class="text-center">Jumlah</th>
					  <th class="text-center">Status Peminjaman</th>
					</tr>
                </thead>
				
                <tbody>
                <?php
                    include "konek.php";
                    $id=$_GET['id_peminjaman'];
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM tb_detail_pinjam INNER JOIN tb_inventaris on tb_detail_pinjam.id_inventaris = tb_inventaris.id_inventaris WHERE id_peminjaman=$id ORDER BY id_detail_pinjam DESC") or die (mysqli_error());
                    $i = 1;
                    while($data = mysqli_fetch_array($query_mysql)){
                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['id_peminjaman']; ?></td>
                                    <td class="text-center"><?php echo $data['nama']; ?></td>
                                    <td class="text-center"><?php echo $data['jumlahp']; ?></td>
                                    <?php if($data ['status_peminjaman'] == 'dipinjam') { ?>
                                   	<td align="center"><font color="red"><?php echo $data['status_peminjaman'] ?></font></td>
                                    <?php } elseif($data ['status_peminjaman'] == 'dikembalikan') { ?>
                                    <td align="center"><font color="blue"><?php echo $data['status_peminjaman'] ?></font></td>
                                    	 <?php } ?>
                                </tr>
								
			<div class="modal fade" id="modal-edit<?php echo $data['id_peminjaman'];?>">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Peminjaman</h4>
					  <div class="modal-body">
						<form method="POST" action="u_detpin.php">
						  <div class="box-body">
							<input type="hidden" name="id_detail_pinjam" value="<?php echo $data['id_peminjaman'] ?>" required>
							<input type="hidden" name="id_peminjaman" value="<?php echo $data['id_peminjaman'] ?>" required>
							<div class="form-group">
							  <label>Status Peminjaman :</label>
								<select name="status_peminjaman" class="form-control" >
									<option value="<?php echo $data['status_peminjaman'] ?>"><?php echo $data['status_peminjaman'] ?></option>
									<option value="dipinjam">Dipinjam</option>
									<option value="dikembalikan">Dikembalikan</option>
								</select>
							</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary" value="update">Simpan Perubahan</button>
						  </div>
						</form>
					  </div>
				    </div>
				  </div>
				</div>
                                <?php
                                    }
                                ?>
                </tbody>
              </table>
			</div>
          </div>
			<div class="modal fade" id="modal-default">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Peminjaman</h4>
				  </div>
				  <div class="modal-body">			  
					<form method="POST" action="prostamb_detpin.php">
					  <div class="box-body">
						<div class="form-group">
						  <label>Nama Inventaris :</label>
								<select name="id_inventaris" class="form-control">
								<option>-</option>
									<?php
										$select=mysqli_query($konek, "SELECT * FROM tb_inventaris");
										while($show=mysqli_fetch_array($select)){
									?>
										<option value="<?=$show['id_inventaris'];?>"><?=$show['nama'];?></option>
									<?php } ?>
								</select>
						</div>
						<div class="form-group">
						  <label>Jumlah :</label>
						  <input name="jumlah" type="number" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Status Peminjaman :</label>
								<select name="status_peminjaman" class="form-control" >
									<option>-</option>
									<option value="dipinjam">Dipinjam</option>
									<option value="dikembalikan">Dikembalikan</option>
								</select>
						</div>
						<div class="form-group">
						  <label>ID Pegawai :</label>
								<select name="id_pegawai" class="form-control">
								<option>-</option>
									<?php
										$select=mysqli_query($konek, "SELECT * FROM tb_peminjaman");
										while($show=mysqli_fetch_array($select)){
									?>
										<option value="<?=$show['id_peminjman'];?>"><?=$show['id_pegawai'];?></option>
									<?php } ?>
								</select>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary" value="Simpan">Simpan</button>
					  </div>
					</form>
				  </div>
				</div>
			  </div>
			</div>
          </div>
		</div>
    </section>
  </div>

<?php
include('modul/footer.php');
?>