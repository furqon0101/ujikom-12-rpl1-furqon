<?php
include 'konek.php';
require('fpdf.php');

date_default_timezone_set("Asia/Jakarta");
$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('lowe.jpg',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'DATA JENIS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telp : (0251)8631261',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. Raya Laladon, Kec. Ciomas, Kab.Bogor 16610',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Website : www.smkn1ciomas.sch.id, Email : smkn1_ciomas@yahoo.co.id',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"LAPORAN DATA JENIS INVENTARIS",0,10,'C');
$pdf->ln(0.5);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date('D-d/m/Y H:i:s'),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(1, 0.8, 'No.', 1, 0, 'C');
$pdf->Cell(8, 0.8, 'Nama Jenis', 1, 0, 'C');
$pdf->Cell(8, 0.8, 'Kode Jenis', 1, 0, 'C');
$pdf->Cell(8, 0.8, 'Keterangan', 1, 1, 'C');
$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($konek, "SELECT * FROM tb_jenis");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(1, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(8, 0.8, $lihat['nama_jenis'],1, 0, 'C');
	$pdf->Cell(8, 0.8, $lihat['kode_jenis'], 1, 0,'C');
	$pdf->Cell(8, 0.8, $lihat['ket'],1, 1, 'C');
	$no++;
}

$pdf->Output("laporan_inventaris.pdf","I");

?>

