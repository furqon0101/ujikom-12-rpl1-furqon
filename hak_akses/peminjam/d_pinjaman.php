<?php
session_start();
if(!isset($_SESSION['username'])){
    die("<script>alert('Silahkan login terlebih dahulu!');document.location.href='../index.php'</script>");//
}
?>
<?php
    include('link.php');
?>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php
      include('menu.php');
    ?>
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          Beranda
          <small>Halaman Peminjam</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="index"><i class="fa fa-home"></i> Beranda</a></li>
          <li><a href="produk"><i class="fa fa-barcode"></i> Data Peminjaman</a></li>
        </ol>
      </section>
      <section class="content">
        <!-- Starts Widget -->
        <?php
          include('widget.php');
        ?>
        <!-- End Widget -->
        <div class="row">

          <div class="col-md-12" style="">
            <div class="box box-primary">
              <div class="box-header with-border">
                <i class="fa fa-cube"></i>
                <h3 class="box-title">Data <small>Peminjaman</small></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>

            <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
          <tr>
            <th class="text-center" style="margin:10px">No</th>
            <th class="text-center">ID Peminjaman</th>
            <th class="text-center">Tanggal Pinjam</th>
            <th class="text-center">Tanggal Kembali</th>
            <th class="text-center">Status Peminjaman</th>
            <th class="text-center">Nama Peminjam</th>
            <th class="text-center">Detail</th>
          </tr>
                </thead>
        
                <tbody>
                <?php                    
                  include "koneksi.php";
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM tb_peminjaman INNER JOIN tb_pegawai on tb_peminjaman.id_pegawai = tb_pegawai.id_pegawai ORDER BY id_peminjaman DESC LIMIT 1") or die (mysqli_error());
                    $i = 1;
                    while($data = mysqli_fetch_array($query_mysql)){
                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['id_peminjaman']; ?></td>
                                    <td class="text-center"><?php echo date ('d F Y', strtotime($data['tanggal_pinjam'])) ?></td>
                                    <td class="text-center"><?php echo date ('d F Y', strtotime($data['tanggal_kembali'])) ?></td>
                                    <?php if($data ['status_peminjaman'] == 'dipinjam') { ?>
                                    <td align="center"><font color="red"><?php echo $data['status_peminjaman'] ?></font></td>
                                    <?php } elseif($data ['status_peminjaman'] == 'dikembalikan') { ?>
                                    <td align="center"><font color="blue"><?php echo $data['status_peminjaman'] ?></font></td>
                                       <?php } ?>
                                    <td class="text-center"><?php echo $data['nama_pegawai']; ?></td>
                                    <td class="text-center">
                                        <a  href="d_pinjaman_detail.php?id_peminjaman=<?php echo $data['id_peminjaman'];?>">
                                  <button  class="btn btn-success btn-sm"><i class="fa fa-eye"></i></button>
                                  </a>
                                    </td>
                                </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <?php
      include('sidebar.php');
    ?>