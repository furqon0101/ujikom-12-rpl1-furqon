<?php
  session_start();
  if(!isset($_SESSION['username'])){
    echo '<script>document.location.href="index.php";</script>';
  }
?>
<?php
error_reporting(0);
if (isset($_SESSION['id_level']))
{
    // jika level admin
    if ($_SESSION['id_level'] == "3")
   {   
   }
   // jika kondisi level karyawan maka akan diarahkan ke halaman lain
   else if ($_SESSION['id_level'] == "2")
   {
       header('location:operator/index.php');
   }
}
if (!isset($_SESSION['id_level']))
{
    header('location:../../../index.php');
}
?>
<?php
	include ('modul/header.php');
?>

  <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Data Peminjam</h1>
			<ol class="breadcrumb">
			   <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
			   <li class="active">Data Peminjam</li>
			</ol>
		</section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default">+Tambah Data</button>
              <button class="btn btn-info" data-toggle="modal" data-target="#modal-info">Print PDF</button>
		        <div class="modal modal-info fade" id="modal-info">
		          <div class="modal-dialog">
		            <div class="modal-content">
		              <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		                  <span aria-hidden="true">&times;</span></button>
		                <h4 class="modal-title">INFORMASI</h4>
		              </div>
		              <div class="modal-body">
		                <p>File ini akan di download, untuk melihat silahkan ke folder download anda! Lanjutkan?&hellip;</p>
		              </div>
		              <div class="modal-footer">
		                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
		                <a href="print_petugas.php"><button type="button" class="btn btn-outline">Lanjutkan..</button></a>
		              </div>
		            </div>
		          </div>
		        </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <div class="table-responsive">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
					<tr>
					  <th class="text-center" style="margin:10px">No</th>
					  <th class="text-center">Tanggal Pinjam</th>
					  <th class="text-center">Tanggal Kembali</th>
					  <th class="text-center">Nama Peminjam</th>
					  <th class="text-center">Opsi</th>
					</tr>
                </thead>
				
                <tbody>
                <?php
                    include "konek.php";
                    $query_mysql = mysqli_query ($konek, "SELECT * FROM tb_peminjaman INNER JOIN tb_pegawai on tb_peminjaman.id_pegawai = tb_pegawai.id_pegawai ORDER BY id_peminjaman DESC") or die (mysqli_error());
                    $i = 1;
                    while($data = mysqli_fetch_array($query_mysql)){
                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td class="text-center"><?php echo $data['tanggal_pinjam']; ?></td>
                                    <td class="text-center"><?php echo $data['tanggal_kembali']; ?></td>
                                    <td class="text-center"><?php echo $data['nama_pegawai']; ?></td>		   <td class="text-center">
                                        <a href="#" data-toggle="modal" data-target="#modal-edit<?php echo $data['id_peminjaman'];?>" class="btn btn-success">Ubah</a>
                                    </td>
                                </tr>
								
			<div class="modal fade" id="modal-edit<?php echo $data['id_peminjaman'];?>">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Edit Peminjaman</h4>
					  <div class="modal-body">
						<form method="POST" action="u_pinjam.php">
						  <div class="box-body">
							<input type="hidden" name="id_peminjaman" value="<?php echo $data['id_peminjaman'] ?>" required>
							<div class="form-group">
							  <label>Tanggal Pinjam :</label>
							  <input name="tanggal_pinjam" type="date" class="form-control" placeholder="" value="<?php echo $data['tanggal_pinjam'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Tanggal Kembali :</label>
							  <input name="tanggal_kembali" type="date" class="form-control" placeholder="" value="<?php echo $data['tanggal_kembali'] ?>" required>
							</div>
							<div class="form-group">
							  <label>Nama Peminjam :</label>
								<select name="id_pegawai" class="form-control" value="<?php echo $data['id_pegawai'] ?>">
									<?php
										$select=mysqli_query($konek, "SELECT * FROM tb_pegawai");
										while($show=mysqli_fetch_array($select)){
									?>
										<option value="<?=$show['id_pegawai'];?>"><?=$show['nama_pegawai'];?></option>
									<?php } ?>
								</select>
							</div>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
							<button type="submit" class="btn btn-primary" value="update">Simpan Perubahan</button>
						  </div>
						</form>
					  </div>
				    </div>
				  </div>
				</div>
                                <?php
                                    }
                                ?>
                </tbody>
              </table>
			</div>
          </div>
			<div class="modal fade" id="modal-default">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Tambah Peminjaman</h4>
				  </div>
				  <div class="modal-body">			  
					<form method="POST" action="prostamb_pinjam.php">
					  <div class="box-body">
						<div class="form-group">
						  <label>Tanggal Kembali :</label>
						  <input name="tanggal_kembali" type="date" class="form-control" placeholder="" required>
						</div>
						<div class="form-group">
						  <label>Nama Peminjam :</label>
							<select name="id_pegawai" class="form-control" required>
								<?php     
									include"konek.php";
									$select=mysqli_query($konek, "SELECT * FROM tb_pegawai");
									while($show=mysqli_fetch_array($select)){
								?>
									<option value="<?=$show['id_pegawai'];?>"><?=$show['nama_pegawai'];?></option>
								<?php } ?>
							</select>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
						<button type="submit" class="btn btn-primary" value="Simpan">Simpan</button>
					  </div>
					</form>
				  </div>
				</div>
			  </div>
			</div>
          </div>
		</div>
    </section>
  </div>

<?php
include('modul/footer.php');
?>